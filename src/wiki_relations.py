# res['name_in_native'] = extract_relation_target_if_exists(claims, 'P1559', 'text')
rel = {"instance_of": 'P31',
       'given_names': 'P735',
       'family_name': 'P734',
       'female form of label': 'P2521',
       'place_of_death': 'P20',
       'place_of_birth': 'P19',
       'positions_held': 'P39',
       'occupation': 'P106',
       'native_label': 'P1705',
       'said to be the same as': 'P460',
       'nationality': 'P1559',
       #ORGANIZATIONS
       'founded by': "P112",
       "chairperson": "P448",
       'secretary general': "P3975",
       "headquaters location": "P159",
       "short name": "P1813",
       "official_name": "P1448",
       #Hierarchy
       "subclass of": "P279"
       }

assert(len(rel.values()) == len(set(rel.values())))
assert(len(rel.keys()) == len(set(rel.keys())))

from common import extract_relation_target_if_exists
from dispatchers.IGenericDispatcher import IGenericDispatcher
from dispatchers.common import ClassDispatcher
from wiki_id import idd
from wiki_relations import rel


class LocationDispatcher(IGenericDispatcher):
    def __init__(self, langs):
        super().__init__(langs,
                         "locations",
                         acceptable_instances=[],
                         acceptable_subclass_of=["locality", 'location', 'spatial entity'],
                         id_map_values={},
                         text_map_values={})
        self.watched_ids["Q7039"] = "Lahr in Switzerland"
        self.watched_ids["Q84"] = "London"
        self.watched_ids["Q30"] = "USA"
        self.watched_ids["Q664"] = "New Zeland"

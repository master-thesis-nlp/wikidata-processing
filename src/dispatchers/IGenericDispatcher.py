from collections import defaultdict

from common import extract_relation_target_if_exists
from dispatchers.allPossibleClassIdDispatcher import compress_id
from dispatchers.common import ClassDispatcher
from wiki_id import idd
from wiki_relations import rel


class IGenericDispatcher(object):
    def __init__(self, langs,
                 out_file_name,
                 acceptable_instances,
                 acceptable_subclass_of,
                 id_map_values,
                 text_map_values):
        self.langs = langs
        self.out_file_name = out_file_name
        self.res_list = {}
        self.acceptable_instances_id = []
        for c in acceptable_instances:
            assert c in idd, "Given wrong instance type = " + str(c)
            self.acceptable_instances_id += [idd[c]]
        for sc in acceptable_subclass_of:
            assert sc in idd, "Given wrong class name = " + str(sc)
        self.acceptable_subclass_of = acceptable_subclass_of
        self.id_map_values = id_map_values
        self.text_map_values = text_map_values
        self.subclass_service = ClassDispatcher()
        self.watched_ids = {}

    def dispatch(self, types, json):
        assert (json["id"] in self.subclass_service.classes), "obj " + json["id"] + " should be in "
        self.on_enter(types, json)
        if not self.is_from_acceptable_class(types, json['id']) or json["id"][0] != "Q":
            self.on_reject(types, json)
            return
        self.on_accept(types, json)
        claims = json['claims']
        res = {}
        for res_n, rel_name in self.id_map_values.items():
            ext = extract_relation_target_if_exists(claims, rel[rel_name])
            if len(ext) > 0:
                res[res_n] = ext
        for res_n, rel_name in self.text_map_values.items():
            ext = extract_relation_target_if_exists(claims, rel[rel_name], 'text')
            if len(ext) > 0:
                res[res_n] = ext
        for l in self.langs:
            labels = []
            if l in json['labels']:
                labels += [json['labels'][l]['value']]
            for el in json['aliases'].get(l, []):
                labels += [el["value"]]
            if len(labels) > 0:
                res['l_' + l] = tuple(labels)
        # res["popularity"] = 0
        res = self.on_add_additional_info(types, json, res)
        self.res_list[compress_id(json['id'])] = dict(res)

    def on_enter(self, types, json):
        for idd, desc in self.watched_ids.items():
            if json['id'] == idd:
                print("on_enter -> ", desc)

    def on_reject(self, types, json):
        for idd, desc in self.watched_ids.items():
            if json['id'] == idd:
                print("on_reject -> ", desc)
                print("parents:", self.subclass_service.classes[idd].get("instance_of", []))

    def on_accept(self, types, json):
        for idd, desc in self.watched_ids.items():
            if json['id'] == idd:
                print("on_accept -> ", desc)

    def on_add_additional_info(self, types, json, res):
        return res

    def is_from_acceptable_class(self, types, obj_id):
        res = False
        for acceptable_instance in self.acceptable_instances_id:
            if acceptable_instance in types:
                res = True
        res2 = False
        if self.subclass_service.is_subclass_of(obj_id, [idd[subclass] for subclass in self.acceptable_subclass_of]):
            res2 = True
        return res2 or res


import pickle
import ujson
from common import extract_relation_target_if_exists, has_relation
from dispatchers.allPossibleClassIdDispatcher import compress_id
from wiki_id import idd
from wiki_relations import rel


class SubclassDispatcher:
    def __init__(self, langs):
        self.langs = langs
        self.ids = pickle.load(open("data/all_possible_ids.pickle", "rb"))
        # self.ids = pickle.load(open("test/s1/all_possible_ids.pickle", "rb"))
        print(len(self.ids))
        self.out_file_name = "subclasses"
        self.res_list = {}

    def dispatch(self, types, json):
        claims = json['claims']
        # if not has_relation(claims, rel['subclass of']):
        #     return
        if compress_id(json['id']) not in self.ids:
            return
        res = {}
        for text_prop in []:
            res[text_prop] = extract_relation_target_if_exists(claims, rel[text_prop], 'text')
        for id_prop in ['subclass of', 'instance_of']:
            res[id_prop] = extract_relation_target_if_exists(claims, rel[id_prop])
        for l in self.langs:
            if l in json['labels']:
                res['l_' + l] = json['labels'][l]['value']
        self.res_list[json['id']] = res

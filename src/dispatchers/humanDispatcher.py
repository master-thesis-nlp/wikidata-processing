from dispatchers.IGenericDispatcher import IGenericDispatcher


class HumanDispatcher(IGenericDispatcher):
    def __init__(self, langs):
        super().__init__(langs,
                         "humans",
                         acceptable_instances=["human", "nationality", "ethnic group"],
                         acceptable_subclass_of=[],
                         id_map_values={# 'names': 'given_names',
                                        # 'fam': 'family_name',
                                        # 'pod': 'place_of_death',
                                        # 'pob': 'place_of_birth',
                                        # 'pos_held': 'positions_held',
                                        'occup': 'occupation'},
                         text_map_values={'nat': 'nationality'})
        # TODO fictional_human and inherited classes (like fictional nationality)

import pickle
import ujson
from common import extract_relation_target_if_exists, has_relation, get_relations_target
from wiki_id import idd
from wiki_relations import rel


def compress_id(idd):
    return idd
    # if idd[0] != "Q":
    #     print("Got totally different id: ", idd)
    # assert idd[0] == "Q"
    # return int(idd[1:])


class AllPossibleClassIdDispatcher:
    def __init__(self, langs):
        self.langs = langs
        self.out_file_name = "all_possible_ids"
        self.res_list = set()

    def dispatch(self, types, json):
        claims = json['claims']
        if has_relation(claims, rel['subclass of']):
            target_id = get_relations_target(claims, rel['subclass of'])
            self.res_list.add(compress_id(json['id']))
            for ids in target_id:
                self.res_list.add(compress_id(ids))
        if has_relation(claims, rel['instance_of']):
            target_id = get_relations_target(claims, rel['instance_of'])
            for ids in target_id:
                self.res_list.add(compress_id(ids))



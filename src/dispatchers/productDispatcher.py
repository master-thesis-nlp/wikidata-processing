from common import extract_relation_target_if_exists
from dispatchers.IGenericDispatcher import IGenericDispatcher
from dispatchers.allPossibleClassIdDispatcher import compress_id
from dispatchers.common import ClassDispatcher
from wiki_id import idd
from wiki_relations import rel


class ProductDispatcher(IGenericDispatcher):
    def __init__(self, langs):
        super().__init__(langs,
                         "products",
                         acceptable_instances=[],
                         acceptable_subclass_of=["type of manufactured good",
                                                 "tangible good",
                                                 "broadcasting program",
                                                 "intellectual work",
                                                 "television station",
                                                 ],
                         id_map_values={},
                         text_map_values={})
        self.watched_ids[compress_id("Q9531")] = "BBC"
        self.watched_ids[compress_id("Q2385689")] = "TVN24"
        self.watched_ids[compress_id("Q208875")] = "The sun"

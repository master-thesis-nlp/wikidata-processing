from common import extract_relation_target_if_exists
from dispatchers.IGenericDispatcher import IGenericDispatcher
from dispatchers.common import ClassDispatcher
from wiki_id import idd
from wiki_relations import rel


class EventDispatcher(IGenericDispatcher):
    def __init__(self, langs):
        super().__init__(langs,
                         "events",
                         acceptable_instances=[],
                         acceptable_subclass_of=["event", "social phenomenon", "occurance"],  # TODO remove and fill
                         id_map_values={},
                         text_map_values={})
        self.watched_ids["Q7888194"] = "Brexit"

import pickle
import ujson
from common import extract_relation_target_if_exists
from dispatchers.IGenericDispatcher import IGenericDispatcher
from wiki_id import idd
from wiki_relations import rel


class NameDispatcher(IGenericDispatcher):
    def __init__(self, langs):
        super().__init__(langs,
                         "names",
                         acceptable_instances=['male given name', 'female given name'],
                         acceptable_subclass_of=[],
                         id_map_values={'said to be the same as': 'said to be the same as'},
                         text_map_values={'native_label': 'native_label'})

    def on_add_additional_info(self, types, json, res):
        sex = 'male' if idd['male given name'] in types else 'female'
        res["sex"] = sex
        return res

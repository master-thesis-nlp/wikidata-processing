from dispatchers.IGenericDispatcher import IGenericDispatcher


class OrganizationDispatcher(IGenericDispatcher):
    def __init__(self, langs):
        super().__init__(langs,
                         "organizations",
                         acceptable_instances=[],
                         acceptable_subclass_of=["organization", "trade agreement", "company"],
                         id_map_values={
                             # 'founded by': 'founded by',
                                        # "chairperson": "chairperson",
                                        # 'secretary general': 'secretary general',
                                        # "headquaters location": "headquaters location"
                         },
                         text_map_values={"short name": "short name", "official_name": "official_name"})
        self.watched_ids["Q1366688"] = "Trans-Pacific Partnership"
        self.watched_ids["Q22079692"] = "Leave.EU"
        self.watched_ids["Q11007"] = "House of Lords"

import pickle

from singleton_decorator import singleton

from common import extract_relation_target_if_exists
from wiki_relations import rel


@singleton
class ClassDispatcher:
    def __init__(self):
        self.winning_path = []
        try:
            with open("data/subclasses.pickle", "rb") as f:
                self.classes = pickle.load(f)
        except FileNotFoundError:
            self.classes = []
        self.build_graph()

    def build_graph(self):
        pass #obsolete

    @staticmethod
    def dispatchClass(json):
        res = {}
        claims = json['claims']
        for text_prop in []:
            res[text_prop] = extract_relation_target_if_exists(claims, rel[text_prop], 'text')
        for id_prop in ['subclass of', 'instance_of']:
            res[id_prop] = extract_relation_target_if_exists(claims, rel[id_prop])
        return json['id'], res

    def add_object_if_not_present(self, json):
        if json['id'] in self.classes:
            return False
        else:
            idd, obj = self.dispatchClass(json)
            self.classes[idd] = obj
            return True

    def is_subclass_of(self, obj_id, classes):
        parent = {}
        queue = []
        classes = set(classes)
        for parent_class in self.classes[obj_id].get("instance_of", []):
            parent[parent_class] = obj_id
            queue += [parent_class]
            if parent_class in classes:
                self.winning_path = [str(parent_class)]
                return True
        while len(queue) > 0:
            act_id = queue[-1]
            queue.pop()
            if act_id not in self.classes:
                continue
            act = self.classes[act_id]
            for n in act.get("subclass of", []):
                if n not in parent:
                    parent[n] = act_id
                    queue = [n] + queue
                if n in classes:
                    x = n
                    self.winning_path = []
                    while x != obj_id and x != -1:
                        self.winning_path += [str(x) + " " + self.classes[x].get('l_pl', "")]
                        x = parent[x]
                    return True
        return False

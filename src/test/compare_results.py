import glob, os
import pickle
import sys


def compare_two_pickles(test, exp, ans):
    exp = pickle.load(open(exp, "rb"))
    ans = pickle.load(open(ans, "rb"))
    exp_k = set(list(exp))
    ans_k = set(list(ans))
    # if type(list(exp_k)[0]) == int:
    #     exp_k = set(["Q" + str(x) for x in exp_k])
    # if type(list(ans_k)[0]) == int:
    #     ans_k = set(["Q" + str(x) for x in ans_k])
    ex = False
    if len(exp_k.difference(ans_k)) > 0:
        dif = list(exp_k.difference(ans_k))
        s = len(dif)
        if len(dif) > 10:
            dif = dif[0:10]
        print(test, "Missing objects[", s,"] of ", len(exp_k), "with id", dif)
        ex = True
    if len(ans_k.difference(exp_k)) > 0:
        dif = list(ans_k.difference(exp_k))
        s = len(dif)
        if len(dif) > 10:
            dif = dif[0:10]
        print(test, "Got overhead[", s,"] with id", dif)
        ex = True
    if ex:
        exit(1)
    if ans != exp:
        print(test, "Got difference")
        if type(exp) == dict:
            for key, value in exp.items():
                # if "canonical_id" in value:
                #     del value["canonical_id"]
                if ans[key] != value:
                    print("Got on object ", key)
                    print("Exp:", value)
                    print("Got:", ans[key])
                    exit(2)
        elif type(exp) == set:
            print("Overhead", exp.difference(ans))
            print("Missing", ans.difference(exp))
            exit(2)


# for d in ["s1/", "s2/", "s3/"]:
d = "s" + sys.argv[1] + "/"
print(len(glob.glob("test/" + d + "*.pickle_exp")), " vs ", len(glob.glob("test/" + d + "*.pickle")))
assert (len(glob.glob("test/" + d + "*.pickle_exp")) == len(glob.glob("test/" + d + "*.pickle")))
for file in glob.glob("test/" + d + "*.pickle_exp"):
    file_name = file.split(".")[0]
    only_file_name = file_name.split("/")[2]
    compare_two_pickles(only_file_name,
                        exp=file_name + ".pickle_exp",
                        ans=file_name + ".pickle")

import glob
from shutil import copyfile

for d in ["s1/", "s2/", "s3/", "s4/"]:
    # print(len(glob.glob("test/"+d+"*.pickle_exp")), " vs ", len(glob.glob("test/"+d+"*.pickle")))
    # assert (len(glob.glob("test/"+d+"*.pickle_exp")) == len(glob.glob("test/"+d+"*.pickle")))
    for file in glob.glob("test/"+d+"*.pickle"):
        file_name = file.split(".")[0]
        target = file_name + ".pickle_exp"
        source = file_name + ".pickle"
        copyfile(source, target)

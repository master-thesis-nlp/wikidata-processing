#!/usr/bin/env bash
set -u
set -e
echo "Running test"
set +e
rm  ./test/s1/*.pickle
rm  ./test/s2/*.pickle
rm  ./test/s3/*.pickle
set -e
pv './test/sample_input1._json' | python3 analyse.py  1 "test/s1"
python3 test/compare_results.py 1
pv './test/sample_input1._json' | python3 analyse.py  2 "test/s2"
python3 test/compare_results.py 2
pv './test/sample_input1._json' | python3 analyse.py  3 "test/s3"
python3 test/compare_results.py 3
pv './test/sample_input1._json' | python3 analyse.py  4 "test/s4"
python3 test/compare_results.py 4
echo "Done testing"
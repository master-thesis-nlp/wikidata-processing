import pickle
import re
import sys
import traceback

import ujson

# pbzip ~1000-1200
# raw file ~7000-8000
from common import get_relations_target, has_relation
from dispatchers.allPossibleClassIdDispatcher import AllPossibleClassIdDispatcher
from dispatchers.common import ClassDispatcher
from dispatchers.eventDispatcher import EventDispatcher
from dispatchers.humanDispatcher import HumanDispatcher
from dispatchers.locationDispatcher import LocationDispatcher
from dispatchers.nameDispatcher import NameDispatcher
from dispatchers.organizationDispatcher import OrganizationDispatcher
from dispatchers.productDispatcher import ProductDispatcher
from dispatchers.subclassDispatcher import SubclassDispatcher
from wiki_relations import *

langs = ['en', 'pl', 'cs', 'ru', 'bg']
dispatchers = []
buldingHierarchy = sys.argv[1] == "1"
buildingClasses = sys.argv[1] == "2"
# STEP 1
if buldingHierarchy:
    print("------------------------------|running step 1")
    dispatchers = [AllPossibleClassIdDispatcher(langs)]
# STEP 2
elif buildingClasses:
    print("------------------------------|running step 2")
    dispatchers = [SubclassDispatcher(langs)]
# STEP 3
elif sys.argv[1] == "3":
    print("------------------------------|running step 3")
    dispatchers = [HumanDispatcher(langs), LocationDispatcher(langs), NameDispatcher(langs)]
# STEP 4
elif sys.argv[1] == "4":
    print("------------------------------|running step 4")
    dispatchers = [OrganizationDispatcher(langs)]
elif sys.argv[1] == "5":
    print("------------------------------|running step 5")
    dispatchers = [ProductDispatcher(langs)]
elif sys.argv[1] == "6":
    print("------------------------------|running step 6")
    dispatchers = [EventDispatcher(langs)]
else:
    print("Unknow variant: ", sys.argv[1])
file_outs = [disp.out_file_name for disp in dispatchers]
class_dispatcher = ClassDispatcher()
assert (len(file_outs) == len(set(file_outs)))
try:
    for line in sys.stdin:
        line = line.strip(" \t\n,][")
        if line == "":
            continue
        # ids = re.findall("\"Q[0-9]+\"", line)
        # print(ids)
        obj = ujson.loads(line)
        claims = obj['claims']
        if has_relation(claims, rel['instance_of']):
            types = get_relations_target(claims, rel['instance_of'])
        else:
            types = []
        if not buldingHierarchy and not buildingClasses:
            added = class_dispatcher.add_object_if_not_present(obj)
            for dispatcher in dispatchers:
                dispatcher.dispatch(types, obj)
            if added:
                del class_dispatcher.classes[obj['id']]
except Exception as e:
    traceback.print_exc()
    print("Exception!!!")
    # raise e
except:
    # raise
    pass
print("Writing files...")
directory = sys.argv[2] if len(sys.argv) > 2 else "data"
for dispatcher in dispatchers:
    file_out = directory + '/' + dispatcher.out_file_name
    print("Saving " + file_out)
    with open(file_out + '.pickle', 'wb') as f:
        print("Writing ", len(dispatcher.res_list))
        pickle.dump(dispatcher.res_list, f)
print("Done")

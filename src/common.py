from wiki_relations import rel


def has_relation(d, rel):
    return rel in d


def get_relations_target(d, relation):
    res = []
    for rel in d[relation]:
        if 'datavalue' in rel['mainsnak'] and 'value' in rel['mainsnak']['datavalue']:
            res += [rel['mainsnak']['datavalue']['value']['id']]
    return res


def extract_relation_target_if_exists(claim, relation, type='id'):
    if has_relation(claim, relation):
        res = []
        for val in claim[relation]:
            if 'datavalue' in val['mainsnak']:
                if type not in val['mainsnak']['datavalue']['value']:
                    print("Type:", type)
                    print("relation ", relation)
                    for rel_name, rel_code in rel.items():
                        if rel_code == relation:
                            print("relation desc: ", rel_name)
                    print(val['mainsnak'])
                    exit(3)
                res += [val['mainsnak']['datavalue']['value'][type]]
        return tuple(res)
    return tuple([])

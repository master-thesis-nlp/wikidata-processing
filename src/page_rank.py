import itertools
import pickle
import re
import sys

pr = set()
try:
    print("Loading ids")
    for path in ["humans.pickle", "events.pickle", "locations.pickle", "organizations.pickle"]:
        d = pickle.load(open("data/" + path, "rb"))
        for idd in d.keys():
            if type(idd) == str:
                if idd[0] == "Q":
                    idd = idd[1:]
                idd = int(idd)
            pr.add(idd)
    pr = dict.fromkeys(pr, 0)
    print("Processing file")
    # ind = 0
    for line in sys.stdin:
        line = line.strip(" \t\n,][")
        if line == "":
            continue
        occurances = re.findall(r"Q[0-9]+\"", line)
        for idd in occurances:
            idd = idd[:-1]
            idd = int(idd[1:])
            if idd in pr:
                pr[idd] += 1
        # if ind > 100:
        #     break
        # ind += 1

except:
    # raise
    pass
# for key, num in pr.items():
#     if num > 0:
#         print(key, num)
file_out = 'src/data/page_rank.pickle'
print("Saving " + file_out)
with open(file_out, 'wb+') as f:
    pickle.dump(pr, f)

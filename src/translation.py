import sys

import ujson
res = ""
cnt = 0
for line in sys.stdin:
    line = line.strip(" \t\n,][")
    if line == "":
        continue
    # ids = re.findall("\"Q[0-9]+\"", line)
    # print(ids)
    obj = ujson.loads(line)
    claims = obj['claims']
    langs = ["pl", "cs", "ru", "bg"]
    line = ""
    nullable = 0
    for l in langs:
        if l in obj["labels"]:
            word = obj['labels'][l]['value']
        else:
            nullable += 1
            word = "NULL"
        if line != "":
            line += "\t"
        line += word
    if nullable < 3:
        print(line)